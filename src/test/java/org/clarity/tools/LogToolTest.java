package org.clarity.tools;

import org.junit.Test;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.assertTrue;

public class LogToolTest
{
    @Test
    public void matinaConex()
    {
        final ByteArrayOutputStream myOut = new ByteArrayOutputStream();
        System.setOut(new PrintStream(myOut));
        String args[] = {  "./resources/input-file-10000.txt", "Matina", "1565647212986", "1565733587895"};
        String expectecOutput = "Evontae";
        LogTool.main(args);
        final String standardOutput = myOut.toString();
        assertTrue(standardOutput.contains(expectecOutput));
    }

}
