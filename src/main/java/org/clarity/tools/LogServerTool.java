package org.clarity.tools;

import java.io.*;
import java.nio.file.FileSystemNotFoundException;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author oarroyo
 */

public class LogServerTool implements Runnable {


    private String toServerName;
    private String fromServerName;
    private File logFile = null;
    private long runInterval;
    private long filePointer = 0;
    private boolean executeAlgorithm = true;
    private String SPLITCHAR;

    private final static String CONNECTTEDTO = "connected to: ";
    private final static String RECEIVEDCONNECTIONS = " received connections from: ";
    private final static String MOSTCONNECTIONS = "the hostname that generated most connections in the last hour: ";
    private final static String NOELEMENTS = "No entries found that match selection criteria ";

    public static void main(String args[]) {
        ExecutorService logExecutor = Executors.newFixedThreadPool(4);
        String filePath = args[0];
        LogServerTool logServerObject = new LogServerTool(args[0], Long.parseLong(args[1]), args[2], args[3], " ");
        logExecutor.execute(logServerObject);
    }

    public LogServerTool(String myFile, long runInterval, String fromServerName, String toServerName, String splitChar) {
        this.logFile = new File(myFile);
        this.runInterval = runInterval;
        this.fromServerName = fromServerName;
        this.toServerName = toServerName;
        this.SPLITCHAR = splitChar;
    }

    public void run() {
        String splitedFileLine[];
        String logLine = null;
        long timeStampMillis = Instant.now().toEpochMilli();
        long oneHourAgoTimeStampMillis = timeStampMillis - 3600000;
        long eventTimeStamp;
        HashSet<String> hsFrom = new HashSet<String>();
        HashSet<String> hsTo = new HashSet<String>();
        List<String> list = new ArrayList<>();
        StringBuilder sb = new StringBuilder();
        try {
            RandomAccessFile readAccessFile = new RandomAccessFile(logFile, "r");
            while (executeAlgorithm) {
                long fileLength = logFile.length();
                if (fileLength > filePointer) {
                    readAccessFile.seek(filePointer);
                    while ((logLine = readAccessFile.readLine()) != null) {
                        splitedFileLine = logLine.split(SPLITCHAR);
                        try {
                            eventTimeStamp = Long.parseLong(splitedFileLine[0]);
                            if (eventTimeStamp > oneHourAgoTimeStampMillis && eventTimeStamp < timeStampMillis) {
                                if (splitedFileLine[2].matches(toServerName)) hsFrom.add(splitedFileLine[1]);
                                if (splitedFileLine[1].matches(fromServerName)) hsTo.add(splitedFileLine[2]);
                                list.add(splitedFileLine[1]);
                            }
                        } catch (NumberFormatException e) {
                            e.printStackTrace();
                        }
                    }
                    filePointer = readAccessFile.getFilePointer();
                    try {
                        Map<String, Long> counted = list.stream()
                                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
                        System.out.println(sb.append(hsFrom).append(CONNECTTEDTO).append(toServerName));
                        sb.setLength(0);
                        System.out.println(sb.append(hsTo).append(RECEIVEDCONNECTIONS).append(fromServerName));
                        sb.setLength(0);
                        System.out.println(sb.append(MOSTCONNECTIONS).append(Collections.max(counted.entrySet(), Map.Entry.comparingByValue()).getKey()));
                    } catch (NoSuchElementException e) {
                        System.out.println(NOELEMENTS);
                    }
                }
                Thread.sleep(runInterval);
            }
        } catch (FileSystemNotFoundException | IOException | InterruptedException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }


}