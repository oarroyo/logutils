package org.clarity.tools;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.HashSet;

/**
 * @author oarroyo
 */

public class LogTool {

    public static void main(String args[]) {
        if (args.length == 0) {
            System.out.println("Pls insert correct args logfilename server timeFrom timeTo");
            System.exit(0);
        }
        staticProcess(new File(args[0]), args[1], Long.parseLong(args[2]), Long.parseLong(args[3]));
    }

    public static void staticProcess(File logFile, String serverName, long timeFrom, long timeTo) {
        String splitedFileLine[];
        long eventTimeStamp;
        String SPLITCHAR = " ";

        HashSet<String> hs = new HashSet<String>();
        RandomAccessFile readAccessFile = null;
        try {
            readAccessFile = new RandomAccessFile(logFile, "r");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        String logFileLine = null;
        try {
            while ((logFileLine = readAccessFile.readLine()) != null) {
                splitedFileLine = logFileLine.split(SPLITCHAR);
                eventTimeStamp = Long.parseLong(splitedFileLine[0]);
                if (eventTimeStamp > timeFrom && eventTimeStamp < timeTo && splitedFileLine[2].matches(serverName)) {
                    hs.add(splitedFileLine[1]);
                }
            }
            readAccessFile.close();
            System.out.println(hs);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
