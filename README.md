# logutil

Just test performance algorithms reading log file

Project is mavenized. Use stardard goals
`mvn clean package`


* LogServerTool use example:

`java -cp ./log-utils-1.0.0.jar org.clarity.tools.LogServerTool ./input-file-name.txt milliseconds fromConectionSever toConnectionServer `


* LogTool use example:

`java -cp /home/vagrant/logutil/target/log-utils-1.0.0.jar org.clarity.tools.LogTool ./input-file-10000.txt Matina 1565647212986 1565733587895`

Enjoy!